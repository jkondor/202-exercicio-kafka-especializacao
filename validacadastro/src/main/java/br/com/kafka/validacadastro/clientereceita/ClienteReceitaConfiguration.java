package br.com.kafka.validacadastro.clientereceita;

import feign.Feign;
import feign.Response;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteReceitaConfiguration {
    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new ClienteReceitaErrorDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .build();
        return Resilience4jFeign.builder(decorators);
    }
}
