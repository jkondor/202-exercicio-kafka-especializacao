package br.com.kafka.validacadastro.clientereceita;

import br.com.kafka.validacadastro.exception.ClientNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteReceitaErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status()==404){
            return new ClientNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
