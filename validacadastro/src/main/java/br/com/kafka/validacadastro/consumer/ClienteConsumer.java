package br.com.kafka.validacadastro.consumer;

import br.com.kafka.validacadastro.clientereceita.ClienteReceita;
import br.com.kafka.validacadastro.clientereceita.ClienteReceitaDTO;
import br.com.kafka.validacadastro.service.ClienteCadastroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class ClienteConsumer {

    @Autowired
    private ClienteCadastroService clienteCadastroService;

    @KafkaListener(topics = "spec2-joao-augusto-2", groupId = "1")
    public void receber(@Payload String cnpj) {

        System.out.println("Recebi o cnpj " + cnpj);
        clienteCadastroService.cadastrarCliente(cnpj);
    }
}
