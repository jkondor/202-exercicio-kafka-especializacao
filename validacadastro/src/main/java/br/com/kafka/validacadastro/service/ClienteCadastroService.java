package br.com.kafka.validacadastro.service;

import br.com.kafka.validacadastro.clientereceita.ClienteReceita;
import br.com.kafka.validacadastro.clientereceita.ClienteReceitaDTO;
import br.com.kafka.validacadastro.model.ClienteCadastro;
import br.com.kafka.validacadastro.producer.ClienteCadastroProducer;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteCadastroService {

    @Autowired
    private ClienteReceita clienteReceita;
    @Autowired
    private ClienteCadastroProducer clienteCadastroProducer;

    public void cadastrarCliente(String cnpj){

        ClienteReceitaDTO clienteReceitaDTO = clienteReceita.getByCNPJ(cnpj);

        System.out.println("Nome Empresa: " + clienteReceitaDTO.getNome());
        System.out.println("Capital Social: " + clienteReceitaDTO.getCapital_social());
        System.out.println("CNPJ: " + clienteReceitaDTO.getCnpj());

        ClienteCadastro clienteCadastro = new ClienteCadastro();
        clienteCadastro.setNome(clienteReceitaDTO.getNome());
        clienteCadastro.setCnpj(clienteReceitaDTO.getCnpj());
        clienteCadastro.setCapital_social(clienteReceitaDTO.getCapital_social());

        if (Double.parseDouble(clienteCadastro.getCapital_social()) >1000000.00){
            clienteCadastro.setAprovado(true);
        } else{
            clienteCadastro.setAprovado(false);
        }

        System.out.println("CLIENTE CADASTRO");
        System.out.println("Nome Empresa: " + clienteCadastro.getNome());
        System.out.println("Capital Social: " + clienteCadastro.getCapital_social());
        System.out.println("CNPJ: " + clienteCadastro.getCnpj());
        if (clienteCadastro.getAprovado()==true){
            System.out.println("Cliente Aprovado");
        } else {
            System.out.println("Cliente Reprovado");
        }

        clienteCadastroProducer.enviarAoKafka(clienteCadastro);

    }
}
