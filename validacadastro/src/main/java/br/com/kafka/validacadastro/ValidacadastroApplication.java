package br.com.kafka.validacadastro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ValidacadastroApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValidacadastroApplication.class, args);
	}

}
