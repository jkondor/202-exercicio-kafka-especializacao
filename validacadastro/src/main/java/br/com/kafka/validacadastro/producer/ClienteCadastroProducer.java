package br.com.kafka.validacadastro.producer;

import br.com.kafka.validacadastro.model.ClienteCadastro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ClienteCadastroProducer {
    @Autowired
    private KafkaTemplate<String, ClienteCadastro> producer;

    public void enviarAoKafka( ClienteCadastro clienteCadastro) {
        producer.send("spec2-joao-augusto-3", clienteCadastro);
    }
}
