package br.com.kafka.validacadastro.clientereceita;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", url = "https://www.receitaws.com.br/v1/cnpj/")
public interface ClienteReceita {

    @GetMapping("/{cnpj}")
    ClienteReceitaDTO getByCNPJ(@PathVariable String cnpj);
}
