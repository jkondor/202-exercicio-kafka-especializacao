package br.com.kafka.gravarcadastro.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_MODIFIED, reason = "Cliente não foi salvo")
public class ClientCadastroNotSaveException extends RuntimeException {
}
