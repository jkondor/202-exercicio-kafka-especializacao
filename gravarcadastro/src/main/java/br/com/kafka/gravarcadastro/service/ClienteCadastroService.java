package br.com.kafka.gravarcadastro.service;

import br.com.kafka.gravarcadastro.exception.ClientCadastroNotSaveException;
import br.com.kafka.validacadastro.model.ClienteCadastro;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.channels.FileLockInterruptionException;

@Service
public class ClienteCadastroService {

    public void gravarArquivoCadastro (ClienteCadastro clienteCadastro) throws IOException {

        try {
            FileWriter fw = new FileWriter("cadastro.csv", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);


            pw.println(clienteCadastro.getNome() + "," + clienteCadastro.getCnpj()+ "," + clienteCadastro.getCapital_social() + "," + clienteCadastro.getAprovado());
            pw.flush();
            pw.close();
        }catch(Exception E) {
            throw new ClientCadastroNotSaveException();
        }
    }
}
