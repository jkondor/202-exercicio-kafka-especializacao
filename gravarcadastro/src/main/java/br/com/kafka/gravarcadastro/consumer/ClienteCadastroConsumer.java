package br.com.kafka.gravarcadastro.consumer;

import br.com.kafka.gravarcadastro.service.ClienteCadastroService;
import br.com.kafka.validacadastro.model.ClienteCadastro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.channels.FileLockInterruptionException;

@Component
public class ClienteCadastroConsumer {
    @Autowired
    ClienteCadastroService clienteCadastroService;

    @KafkaListener(topics = "spec2-joao-augusto-3", groupId = "1")
    public void receber(@Payload ClienteCadastro clienteCadastro) throws IOException {

        System.out.println("Recebi o cliente: " + clienteCadastro.getNome());
        System.out.println("Recebi o CNPJ: " + clienteCadastro.getCnpj());
        System.out.println("Recebi o capital social: " + clienteCadastro.getCapital_social());
        System.out.println("Recebi indicação de aprovado: " + clienteCadastro.getAprovado());

        clienteCadastroService.gravarArquivoCadastro(clienteCadastro);

    }
}
