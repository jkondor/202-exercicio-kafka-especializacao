package br.com.kafka.gravarcadastro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GravarcadastroApplication {

	public static void main(String[] args) {
		SpringApplication.run(GravarcadastroApplication.class, args);
	}

}
