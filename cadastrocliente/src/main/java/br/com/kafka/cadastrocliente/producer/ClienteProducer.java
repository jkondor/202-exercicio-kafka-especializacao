package br.com.kafka.cadastrocliente.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ClienteProducer {

    @Autowired
    private KafkaTemplate<String, String> producer;

    public void enviarAoKafka( String cnpj) {
        producer.send("spec2-joao-augusto-2", cnpj);
    }
}
