package br.com.kafka.cadastrocliente.controller;

import br.com.kafka.cadastrocliente.producer.ClienteProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClienteController {

    @Autowired
    private ClienteProducer clienteProducer;

    @PostMapping("/cadastro/{cnpj}")
    public void castrarCliente(@PathVariable String cnpj){

        System.out.println("CNPJ cadastrado: " + cnpj);
        clienteProducer.enviarAoKafka(cnpj);
    }
}
